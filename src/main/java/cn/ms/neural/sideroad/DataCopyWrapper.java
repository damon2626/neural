package cn.ms.neural.sideroad;

public interface DataCopyWrapper<T> {

	void wrapper(T input, T data);

}
